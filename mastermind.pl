% nbBienPlace(+Code1, +Code2, -BP)

nbBienPlace([],[],0).

nbBienPlace([H1|T1],[H2|T2],BP):-
    H1 \= H2,
    nbBienPlace(T1,T2,BP).

nbBienPlace([H|T1],[H|T2],BP):-
    nbBienPlace(T1,T2,BP2),
    BP is BP2+1.

/*
?- nBienPlace([1,2,3,4], [1,2,3,5], BP).
Correct to: "nbBienPlace([1,2,3,4],[1,2,3,5],BP)"?
Please answer 'y' or 'n'? yes
BP = 3 .
*/

% longueur(+L, -N)

longueur([],0).
longueur([_|T],N):-
    longueur(T,N1),
    N is N1+1.

% gagne(+Code1, +Code2)
gagne(Code1,Code2):-
    nbBienPlace(Code1,Code2,N),
    longueur(Code1,N).

/*
?- gagne([1,2,3,4], [1,2,3,4]).
true.
*/

% element(+E, +L)

element(E,[H|T]):-
    E \= H,
    element(E,T).

/*
?- element(2, [1,2,3,4]).
true
*/

% enleve(+E, +L1, -L2)
enleve(_,[],[]).
enleve(E,[E|L2],L2).
enleve(E,[H1|T1],[E|L2]):-
    E \= H1,
    enleve(E,T1,L2).
/*
?- enleve(2, [1,2,3,4], L2).
L2 = [2, 3, 4]
*/

% enleveBP(+Code1, +Code2, -Code1Bis, -Code2Bis)
enleveBP(_,[],[]).
